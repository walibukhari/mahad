<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" type="text/css" href="taj/style.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Jost&display=swap" rel="stylesheet">
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
</head>

<body>
	<header>
		<nav>
			<div class="logo"> <h1 class="animate__animated infinite animate__heartBeat">Mera Pakistan</h1> </div>

			<div class="menu">
				<a href="#">Home</a>
				<a href="#">Gallery</a>
				<a href="#">About</a>
				<a href="#">Contact</a>
			</div>
		</nav>

		<main>
			<section>
				<h3>Welcome To Pakistan</h3>
				<h1>DO COME & VISIT <span class="change_content"></span>
				</h1>
				<p>"Pakistan once is not enough"</p>
				<a href="#" class="btnone">Learn More</a>
				<a href="#" class="btntwo">SignUp Here</a>
			</section>
		</main>
	</header>
</body>
</html>