<!DOCTYPE html>
<html>

<head>
<title>Full Screen Background Video</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<section class="header">

		<video autoplay loop class="video-background" muted plays-inline>
			<source src="assets/background-video.mp4" type="video/mp4">
		
		</video>

		
	</section>

</body>
</html>