<!DOCTYPE html>
<html>
<head>
	<title>Barber Website</title>
	<link rel="stylesheet" type="text/css" href="barber/style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&family=Poppins&display=swap" rel="stylesheet">
    <meta name="viewport"content="width=device-width,initial-scale =1">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	<section id="banner">
		<img src="/assets/balogo.png" class="logo">

		<div class="banner_text">
			<h1>Hair Studio</h1>
			<p>Style Your Hair Is Style Your Life</p>
		</div>

		<div class="banner-btn">
			<a href="#"><span></span>Find Out</a>
			<a href="#"><span></span>Read More</a>
		</div>
	</section>

	<div id="sideNav">
		<nav>
			<ul>
				<li><a href="#"></a>HOME</li>
				<li><a href="#"></a>FEATURES</li>
				<li><a href="#"></a>SERVICES</li>
				<li><a href="#"></a>TESTIMONIALS</li>
				<li><a href="#"></a>MEET US</li>
			</ul>
		</nav>
	</div>

	<div id="menuBtn">
		<img src="/assets/menu.png" id="menu">
	</div>

	<!-- <--------Features------->

	<section id="features">
		<div class="title-features">
			<p>FEATURES</p>
			<h1>Why Choose Us</h1>
		</div>
		<div class="feature-box">
			<div class="features">
				<h1>Experienced Staff</h1>
                <div class="features-desc">
                    <div class="features-icon">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                    </div>
                    <div class="features-text">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            hello its me...</p>
                    </div>
                </div>
                <h1>Pre Booking Online</h1>
                <div class="features-desc">
                    <div class="features-icon">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    </div>
                    <div class="features-text">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            hello its me...</p>
                    </div>
                </div>
                <h1>Affordable Cost</h1>
                <div class="features-desc">
                    <div class="features-icon">
                        <i class="fa fa-inr" aria-hidden="true"></i>
                    </div>
                    <div class="features-text">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            hello its me...</p>
                    </div>
                </div>
			</div>
            <div class="features-img">
				<img src="/assets/p1.jpg">
			</div>
		</div>
	</section>

{{--    ........servies.......--}}
    <section id="services">
        <div class="title-features">
            <p>SERVICES</p>
            <h1>We Provide Better</h1>
        </div>
        <div class="service-box">
            <div class="single-service">
                <img src="/assets/pexel1.jpg">
                <div class="overlay"></div>
                <div class="service-desc">
                    <h3>Hair Styling</h3>
                    <hr>
                    <p>this is test under description of barber
                    foundation this is under description of barber</p>
                </div>
            </div>
            <div class="single-service">
                <img src="/assets/pexel2.jpg">
                <div class="overlay"></div>
                <div class="service-desc">
                    <h3>Hair Puff</h3>
                    <hr>
                    <p>this is test under description of barber
                        foundation this is under description of barber</p>
                </div>
            </div>
            <div class="single-service">
                <img src="/assets/pexel3.jpg">
                <div class="overlay"></div>
                <div class="service-desc">
                    <h3>Beard Setting</h3>
                    <hr>
                    <p>this is test under description of barber
                        foundation this is under description of barber</p>
                </div>
            </div>
            <div class="single-service">
                <img src="/assets/pexel4.jpg">
                <div class="overlay"></div>
                <div class="service-desc">
                    <h3>Side Cutting</h3>
                    <hr>
                    <p>this is test under description of barber
                        foundation this is under description of barber</p>
                </div>
            </div>
        </div>
    </section>

    <!-- .......tertimonial......... -->
    <section id="testimonial">
    	<div class="title-features">
            <p>TESTIMONIAL</p>
            <h1>Whats Client Says</h1>
        </div>

        <div class="tertimonial-row">
        	<div class="tertimonial-col">
        		<div class="user">
        			<img src="/assets/men1.jpeg">
        			<div class="user-info">
        				<h4>KEN KARMEIN <i class="fa fa-twitter"></i></h4>
        				<small>@Kennoor</small>
        			</div>
        		</div>
        		<p>
        			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis metus purus. Aenean euismod enim eu lacus congue auctor interdum bibendum tellus. Duis nisi mauris.
        		</p>
        	</div>
        	<div class="tertimonial-col">
        		<div class="user">
        			<img src="/assets/men3.jpeg">
        			<div class="user-info">
        				<h4>DAVID JOSEPH<i class="fa fa-twitter"></i></h4>
        				<small>@DaJoseeph</small>
        			</div>
        		</div>
        		<p>
        			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis metus purus. Aenean euismod enim eu lacus congue auctor interdum bibendum tellus. Duis nisi mauris.
        		</p>
        	</div>
        	<div class="tertimonial-col">
        		<div class="user">
        			<img src="/assets/men4.jpeg">
        			<div class="user-info">
        				<h4>ARMER DAVID<i class="fa fa-twitter"></i></h4>
        				<small>@ArmerDavid</small>
        			</div>
        		</div>
        		<p>
        			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis metus purus. Aenean euismod enim eu lacus congue auctor interdum bibendum tellus. Duis nisi mauris.
        		</p>
        	</div>
        </div>
    </section>

    <!-- ......Footer...... -->
    <section id="footer">
    	<img src="/assets/footer.jpg" class="footer-img">
    	<div class="title-features">
            <p>CONTACT</p>
            <h1>Visit Shops Today</h1>
        </div>
        <div class="footer-row">
        	<div class="footer-left">
        		<h1>Opening Hours</h1>
        		<p><i class="fa fa-clock-o"></i>Monday To Friday-10pm to 7pm</p>
        		<p><i class="fa fa-clock-o"></i>Saturday and sunday-Closed</p>
        	</div>
        	<div class="footer-right">
        		<h1>Get In Touch</h1>
        		<p>#Chungi No 1 Multan<i class="fa fa-map-marker"></i></p>
        		<p>bmahad083@gmail.com<i class="fa fa-paper-plane"></i></p>
        		<p>+92 8734687346<i class="fa fa-phone"></i></p>
        	</div>
        </div>

        <div class="social-links">
        	<i class="fa fa-facebook"></i>
        	<i class="fa fa-instagram"></i>
        	<i class="fa fa-youtube-play"></i>
        	<i class="fa fa-twitter"></i>
        	<p>CopyRight Design By @MahadBukhari 2020</p>
        </div>
    </section>
    
	<!-- <-------Slide Nave-------->

	<script>
		var menuBtn = document.getElementById("menuBtn")
		var sideNav = document.getElementById("sideNav")
		var menu = document.getElementById("menu")

		sideNav.style.right = "-250px";

		menuBtn.onclick = function(){
			if(sideNav.style.right == "-250px"){
				sideNav.style.right = "0";
				menu.src="/assets/cancel.png";
			}
			else
			{
				sideNav.style.right = "-250px";
				menu.src="/assets/menu.png";

			}
		}
	</script>

</body>
</html>
