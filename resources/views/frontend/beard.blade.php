<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css3/style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	<header>
		<nav class="nav-bar">

			<div class="logo">
				<a href="#">Thapa Technical</a>
			</div>

			<div class="menu">
				<ul>
					<li><a href="#" class="active">Home</a></li>
					<li><a href="#" >Gallery</a></li>
					<li><a href="#" >Services</a></li>
					<li><a href="#" >Portfolio</a></li>
					<li><a href="#" >About</a></li>
				</ul>
			</div>

			<div class="contact">

				<a href="#">Contact me</a>
				
			</div>
			
		</nav>

		<div class="contact_center">
			<h1>Hello This is Thapa</h1>
			<h2>Youtube/full stack developer</h2>
		</div>
		<div class="social_networks">

			<div class="fa_icons">

				<a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
				
			</div>
			<div class="fa_icons">

				<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
				
			</div>
			<div class="fa_icons">

				<a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
				
			</div>
			<div class="fa_icons">

				<a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
				
			</div>
			
		</div>
	</header>

</body>
</html>