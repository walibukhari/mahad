<!DOCTYPE html>
<html>
<head>
	<title>My Website</title>
	<link rel="stylesheet" type="text/css" href="css2/style.css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
     
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

	<div class="bgimage">

		<nav class="navbar navbar-expand-md bg-dark navbar-dark">

			<div class="container">

				<a href="#" class="navbar-brand text-warning font-weight-bold">SYEDMAHADBUKHARI</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsenavbar">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse text-center" id="collapsenavbar">

					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a href="#" class="nav-link text-white">SERVICES</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link text-white">PORTFOLIO</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link text-white">CONTACTUS</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link text-white">ABOUT</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link text-white">TEAM</a>
						</li>
						
					</ul>
					
				</div>
				
			</div>
			
		</nav>

		<div class="container text-center text-white headerset">
			<h2>Welcome To Our Studio! </h2>
			<h1>IT'S NICE TO MEET YOU..! </h1>
			<button class="btn btn-warning text-white btn-lg">SUBSCRIBE</button>
		</div>
		
	</div>

	<section class="container ourservices text-center">
		
		<h1>SERVICES</h1>
		<p>Sometimes We Feel That All Doors Are Closed </p>

		<div class="row rowSetting">

			<div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto">
				
				<div class="imgSetting d-block m-auto bg-warning"> <i class="fa fa-shopping-cart fa-3x text-white "></i>
				</div>

				<h2>E-COMMERCE</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>

			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto">
				
				<div class="imgSetting d-block m-auto bg-warning"> <i class="fa fa-desktop fa-3x text-white"></i>
				</div>

				<h2>RESPONSIVE</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>

			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto">
				
				<div class="imgSetting d-block m-auto bg-warning"> <i class="fa fa-lock fa-3x text-white"></i>
				</div>

				<h2>WEB-SECURITY</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>

			</div>
			
		</div>
	</section>

<!-- ///////////////////////PORTFOLIO////////////////////	 -->

<section class="portfolio bg-light">

	<div class="container text-center">
		<h2>PORTFOLIO</h2>
		<p>EVERY GREAT THINGS TAKES TIME.....!</p>

		<div class="row">

			<div class="col-lg-4 col-md-4 col-sm-12 col-10 d-block m-auto">
				<div class="card">
					<img src="assets/pc.jpg" class="card-img img-fluid">

					<div class="card-body">
						<h4 class="card-title"> THREADS </h4>
						<p class="card-text"> ILLUSTRATION </p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12 col-10 d-block m-auto">
				<div class="card">
					<img src="assets/pg2.jpg" class="card-img img-fluid">

					<div class="card-body">
						<h4 class="card-title"> EXPLORE </h4>
						<p class="card-text"> GRAPHICS DESIGHN </p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12 col-10 d-block m-auto">
				<div class="card">
					<img src="assets/pg3.jpg" class="card-img img-fluid">

					<div class="card-body">
						<h4 class="card-title"> FINISH </h4>
						<p class="card-text"> IDENTITY </p>
					</div>
				</div>
			</div>
			
		</div>

		<div class="row">

			<div class="col-lg-4 col-md-4 col-sm-12 col-10 d-block m-auto">
				<div class="card">
					<img src="assets/pg4.jpeg" class="card-img img-fluid">

					<div class="card-body">
						<h4 class="card-title"> LINES </h4>
						<p class="card-text"> BRADINGS </p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12 col-10 d-block m-auto">
				<div class="card">
					<img src="assets/pg5.jpeg" class="card-img img-fluid">

					<div class="card-body">
						<h4 class="card-title"> SOUTh </h4>
						<p class="card-text"> WEBSITE DESIGN </p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12 col-10 d-block m-auto">
				<div class="card">
					<img src="assets/pg6.jpeg" class="card-img img-fluid">

					<div class="card-body">
						<h4 class="card-title"> WINDOW </h4>
						<p class="card-text"> PHOTOGRAPHY </p>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
	
</section>

<section class="ourteam">
	<div class="container text-center">
		<h1>OUR AMAZING TEAM</h1>
		<p>YES IF YOU THINK A GOAL THEN YOU DEFINITELY ACHIEVE...!</p>

	<div class="row teamSetting">
		<div class="col-lg-4 col-md-4 col-sm-10 col-12 d-block m-auto">

			<figure class="figure">
				<img src="assets/bm7.jpg" class="figure-img img-fluid rounded-circle" height="250px" width="250px">

				<figcaption>
					<h4>Kay Garland</h4>
					<p class="figure-caption">Lead Designer</p>
				</figcaption>
			</figure>
			
		</div>

		<div class="col-lg-4 col-md-4 col-sm-10 col-12 d-block m-auto">

			<figure class="figure">
				<img src="assets/bm2.jpeg" class="figure-img img-fluid rounded-circle" height="250px" width="250px">

				<figcaption>
					<h4>John David</h4>
					<p class="figure-caption">Lead Designer</p>
				</figcaption>
			</figure>
			
		</div>

		<div class="col-lg-4 col-md-4 col-sm-10 col-12 d-block m-auto">

			<figure class="figure">
				<img src="assets/bm3.jpeg" class="figure-img img-fluid rounded-circle" height="250px" width="250px">

				<figcaption>
					<h4>Jenefir Kay</h4>
					<p class="figure-caption">Lead Designer</p>
				</figcaption>
			</figure>
			
		</div>
	</div>
	</div>
</section>

<footer class="footer">
	<h6 class="text-center">CopyRight @ 2020 , Design By Syed Mahad Bukhari</h6>
</footer>

</body>
</html>