<section>
		<div class="container-fluid">
			<h1 class=" text-effects text-center text-capitalize pt-5">About Us</h1>
			<hr class="w-25 mx-auto pt-5">

			<div class="container my-4 customSetting">
	<div class="row featurette d-flex justify-content-center align-items-center">
          <div class="col-md-7">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5">
            <img class=" img-fluid" data-src="holder.js/350x350/auto" alt="500x500" src="https://source.unsplash.com/350x350?fast,pepsi" data-holder-rendered="true">
          </div>
    </div>
    <div class="row featurette d-flex justify-content-center align-items-center">
          <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5 order-md-1">
            <img class=" img-fluid" data-src="holder.js/350x350/auto" alt="500x500" src="https://source.unsplash.com/350x350?fast,sandwich" data-holder-rendered="true">
          </div>
    </div>
    <div class="row featurette d-flex justify-content-center align-items-center">
          <div class="col-md-7">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5">
            <img class=" img-fluid" data-src="holder.js/350x350/auto" alt="500x500" src="https://source.unsplash.com/350x350?fast,pizza" data-holder-rendered="true">
          </div>
    </div>
</div>
		</div>
	</section>