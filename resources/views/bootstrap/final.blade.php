<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Final Website</title>
	<link rel="stylesheet" type="text/css" href="Final/style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
	<header>
		<a href="#" class="logo">Food<span>.</span></a>
		<div class="menuToggole" onclick="toggleMenu();"></div>
		<ul class="navigation">
			<li><a href="#banner" onclick="toggleMenu();">Home</a></li>
			<li><a href="#about" onclick="toggleMenu();">About</a></li>
			<li><a href="#menu" onclick="toggleMenu();">Menu</a></li>
			<li><a href="#expert" onclick="toggleMenu();">Expert</a></li>
			<li><a href="#testimonials" onclick="toggleMenu();">Testimonials</a></li>
			<li><a href="#contact" onclick="toggleMenu();">Contact</a></li>
		</ul>
	</header>
<section class="banner" id="banner">
	<div class="content">
		<h2>Always choose food</h2>
		<p>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown 
		</p>
		<a href="#" class="btn">Our Menu</a>
	</div>
</section>
<section class="about" id="about">
	<div class="row">
		<div class="col50">
			<h2 class="titleText"><span>A</span>bout Us</h2>
			<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown<br>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
			</p>
		</div>
		<div class="col50">
			<div class="imgBx">
				<img src="assets/pizza56.jpeg" alt="image" class="customSetting">
			</div>
		</div>
	</div>
</section>
<section class="menu" id="menu">
	<div class="title">
		<h2 class="titleText">Our <span>M</span>enu</h2>
		<p>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry.
		</p>
	</div>
	<div class="content">
		<div class="box">
			<div class="imgBx">
				<img src="assets/saladss.jpg" alt="newimage">
			</div>
			<div class="text">
				<h3>Special Salads</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/saladss.jpg" alt="newimage">
			</div>
			<div class="text">
				<h3>Special Soup</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/saladss.jpg" alt="newimage">
			</div>
			<div class="text">
				<h3>Special Pasta</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/saladss.jpg" alt="newimage">
			</div>
			<div class="text">
				<h3>Special Salads</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/saladss.jpg" alt="newimage">
			</div>
			<div class="text">
				<h3>Special Salads</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/saladss.jpg" alt="newimage">
			</div>
			<div class="text">
				<h3>Special Salads</h3>
			</div>
		</div>
	</div>
	<div class="title">
		<a href="#" class="btn">View All</a>
	</div>
</section>
<section class="expert" id="expert">
	<div class="title">
		<h2 class="titleText">Our Kitchen<span>E</span>pert</h2>
		<p>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry.
		</p>
	</div>
	<div class="content">
		<div class="box">
			<div class="imgBx">
				<img src="assets/exper11.jpeg" alt="image">
			</div>
			<div class="text">
				<h3>Someone Famous</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/exper11.jpeg" alt="image">
			</div>
			<div class="text">
				<h3>Someone Famous</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/exper11.jpeg" alt="image">
			</div>
			<div class="text">
				<h3>Someone Famous</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/exper11.jpeg" alt="image">
			</div>
			<div class="text">
				<h3>Someone Famous</h3>
			</div>
		</div>
	</div>
</section>
<section class="testimonials" id="testimonials">
	<div class="title white">
		<h2 class="titleText">They<span>S</span>aid about us</h2>
		<p>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry.
		</p>
	</div>
	<div class="content">
		<div class="box">
			<div class="imgBx">
				<img src="assets/gents11.jpeg" alt="image">
			</div>
			<div class="text">
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.
				</p>
				<h3>Someone famous</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/gents22.jpeg" alt="image">
			</div>
			<div class="text">
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.
				</p>
				<h3>Someone famous</h3>
			</div>
		</div>
		<div class="box">
			<div class="imgBx">
				<img src="assets/gents33.jpeg" alt="image">
			</div>
			<div class="text">
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.
				</p>
				<h3>Someone famous</h3>
			</div>
		</div>
	</div>
</section>
<section class="contact" id="contact">
	<div class="title">
		<h2 class="titleText"><span>C</span>ontact us</h2>
		<p>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry.
		</p>
	</div>
	<div class="contactForm">
		<h3>Send Messages</h3>
		<div class="inputBox">
			<input type="text" name="username" placeholder="Enter Name" id="username">
			<div id="name-error"></div>
		</div>
		<div class="inputBox">
			<input type="text" name="email" placeholder="Enter Email" id="email">
			<div id="email-error"></div>
		</div>
		<div class="inputBox">
			<textarea placeholder="Description"></textarea>
		</div>
		<div class="inputBox">
			<input type="submit" name="submit" value="submit" id="submit">
		</div>
	</div>
</section>

<div class="copyrighttext">
	<p>CopyRight 2020 Design By SyedMahadBukhari</p>
</div>
<script type="text/javascript">
	window.addEventListener('scroll',function(){
      const header = document.querySelector('header');
      header.classList.toggle("sticky", window.scrollY > 0);
	});
	function toggleMenu(){
		const menuToggole = document.querySelector('.menuToggole');
		const navigation = document.querySelector('.navigation');
		menuToggole.classList.toggle('active');
		navigation.classList.toggle('active');
	}
</script>
<script>
	$(document).ready(function(){
		$('#submit').click(function(){
			var usename = $('#username').val();
			if(usename == "")
			{
				$('#name-error').html('Please filled not empty...!');
				$('#name-error').css('color','red').fadeOut(4000);
				return false;
			}
			if((usename.length <= 5) || (usename.length >=12))
			{
				$('#name-error').html('Character length is must between 5 t0 12...');
				$('#name-error').css('color','red').fadeOut(4000);
				return false;
			}
			if(!isNaN(usename))
			{
				$('#name-error').html('only charactered is allowed..!...');
				$('#name-error').css('color','red').fadeOut(4000);
				return false;
			}
		});
		$('#submit').click(function(){
			var useremail = $('#email').val();
			if(useremail == "")
			{
				$('#email-error').html('Email section is also filled not empty...!');
				$('#email-error').css('color','red').fadeOut(4000);
				return false;
			}
		});
	});
</script>
</body>
</html>