<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous">
	</script>
	<title>Html&Css</title>
	<link rel="stylesheet" type="text/css" href="finally/style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
</head>
<body>
<div class="container">
	<div class="forms-container">
		<div class="signin-signup">
			<form action="" class="sign-in-form">
				<h2 class="title">Sign in</h2>
				<div class="input-field">
					<i class="fas fa-user"></i>
					<input type="text" name="" placeholder="username">
				</div>
				<div class="input-field">
					<i class="fas fa-lock"></i>
					<input type="password" name="" placeholder="password">
				</div>
				<input type="submit" value="login" class="btn solid" name="">

				<p class="social-text">or Sign in with social platform</p>
				<div class="social-media">
					<a href="#" class="social-icon">
						<i class="fab fa-facebook-f"></i>
					</a>
					<a href="#" class="social-icon">
						<i class="fab fa-twitter"></i>
					</a>
					<a href="#" class="social-icon">
						<i class="fab fa-google"></i>
					</a>
					<a href="#" class="social-icon">
						<i class="fab fa-linkedin-in"></i>
					</a>
				</div>
			</form>

			<form action="" class="sign-up-form">
				<h2 class="title">Sign un</h2>
				<div class="input-field">
					<i class="fas fa-user"></i>
					<input type="text" name="" placeholder="username">
				</div>
				<div class="input-field">
					<i class="fas fa-envelope"></i>
					<input type="text" name="" placeholder="Email">
				</div>
				<div class="input-field">
					<i class="fas fa-lock"></i>
					<input type="password" name="" placeholder="password">
				</div>
				<input type="submit" value="Sign up" class="btn solid" name="">

				<p class="social-text">or Sign un with social platform</p>
				<div class="social-media">
					<a href="#" class="social-icon">
						<i class="fab fa-facebook-f"></i>
					</a>
					<a href="#" class="social-icon">
						<i class="fab fa-twitter"></i>
					</a>
					<a href="#" class="social-icon">
						<i class="fab fa-google"></i>
					</a>
					<a href="#" class="social-icon">
						<i class="fab fa-linkedin-in"></i>
					</a>
				</div>
			</form>
		</div>
	</div>

	<div class="panels-container">
		<div class="panel left-panel">
			<div class="content">
				<h3>New here ?</h3>
				<p>Loreum mnsdbfuie   snhbgwe  nhsdbfweyjrg wehfgyiwg vwerbghi</p>
				<button class="btn transparent" id="sign-up-btn">Sign Up</button>
			</div>
			<img src="assets/draw1.svg" class="image" alt="image">
		</div>
		<div class="panel right-panel">
			<div class="content">
				<h3>one of us ?</h3>
				<p>Loreum mnsdbfuie   snhbgwe  nhsdbfweyjrg wehfgyiwg vwerbghi</p>
				<button class="btn transparent" id="sign-in-btn">Sign in</button>
			</div>
			<img src="assets/draw22.svg" class="image" alt="image">
		</div>
	</div>
</div>

<script type="text/javascript">
	const sign_in_btn = document.querySelector("#sign-in-btn");
	const sign_up_btn  = document.querySelector("#sign-up-btn");
	const container = document.querySelector(".container");

	sign_up_btn.addEventListener('click', () =>{
		container.classList.add("sign-up-mode");
	});
	sign_in_btn.addEventListener('click', () =>{
         container.classList.remove("sign-in-mode");
	});
</script>
</body>
</html>