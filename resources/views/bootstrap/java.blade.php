<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=width-device, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="java/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com"> 
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Final Html Css</title>
</head>
<body>
<nav class="navbar background h-nav">
    <ul class="nav-list v-class">
        <div class="logo">
            <img src="assets/thunder.jpg" alt="logoimage">
        </div>
        <li><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Services</a></li>
        <li><a href="#">Contact</a></li>
    </ul>
    <div class="right-nav v-class">
        <input type="text" name="search" id="search">
        <button class="btn btn-sm">Search</button>
    </div>
    <div class="burger">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
</nav>
<section class="background firstSection">
    <div class="main-box">
        <div class="firstHalf">
            <p class="big-text">The Future Of Education is Here</p>
            <p class="small-text">if you want to be a front end developer you must have patience other you will be panic and not other will help you.The Future Of Education is Here</p>
            <div class="buttons">
                <button class="btn1">Subscribe</button>
                <button class="btn2">More Videos</button>
            </div>
        </div>
        <div class="secondHalf">
            <img src="assets/build.jpeg" alt="new-image">
        </div>
    </div>
</section>
<section class="section">
    <div class="paras">
        <p class="sectionTag big-text">The end of poverty if here</p>
    <p class="sectionSubTag small-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
    </div>
    <div class="thumbnail">
        <img src="assets/build.jpeg" alt="yes-image" class="img-fluid">
    </div>
</section>
<hr>
<section class="section section-left">
    <div class="paras">
        <p class="sectionTag big-text">The end of poverty if here</p>
    <p class="sectionSubTag small-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
    </div>
    <div class="thumbnail">
        <img src="assets/build.jpeg" alt="yes-image" class="img-fluid">
    </div>
</section>
<hr>
<section class="section">
    <div class="paras">
        <p class="sectionTag big-text">The end of poverty if here</p>
    <p class="sectionSubTag small-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
    </div>
    <div class="thumbnail">
        <img src="assets/build.jpeg" alt="yes-image" class="img-fluid">
    </div>
</section>
<section class="contact">
    <h2>Contact Us</h2>
    <form action="#">
    <div class="form">
        <input type="text" name="name" id="name" placeholder="Enter Your Name plz">
        <br>
        <div id="name-error"></div>
        <input type="number" name="phone" id="phone" placeholder="Enter your number plz">
        <br>
        <div id="number-error"></div>
        <input type="email" name="email" id="email" placeholder="enter your email plz">
        <br>
        <div id="email-error"></div>
        <textarea class="form-description" id="text" name="text" cols="30" rows="10" placeholder="Enter Your description"></textarea>
        <br>
         <div id="descrip-error"></div>
        <input type="submit" name="submit" id="submit" style="background-color: dodgerblue; color: white;">
    </div>
 </form>
</section>
<footer>
    <div class="text-footer">
        <h1>CopyRight@ Design by syed mahad bukhari</h1>
    </div>
</footer>

<script src="js/style.js"></script>
<script>
    $(document).ready(function(){
        $('#submit').click(function(){
            var username = $('#name').val();
            if(username == ""){
                $('#name-error').html('username is not empty please filled it...!');
                $('#name-error').css('color','red').fadeOut(3000);
                return false;
            }
            if((username.lenght <= 5) || (username.lenght >= 12))
            {
                $('#name-error').html('username lenght is must between 5 to 12 character...!');
                $('#name-error').css('color','green').fadeOut(4000);
                return false;
            }
            if(!isNaN(username))
            {
                $('#name-error').html('only charactered is allowed..!');
                $('#name-error').css('color','purple').fadeOut(4000);
            }
        });
        $('#submit').click(function(){
            var phonenum = $('#phone').val();
            if(phonenum == ""){
                $('#number-error').html('number is not empty..! please filled it');
                $('#number-error').css('color','red').fadeOut(3000);
                return false;
            }
            if((phonenum.lenght <=5) || (phonenum.lenght >= 12)){
                $('#number-error').html('digits lenght is must between 5 to 12');
                $('#number-error').css('color','dodgerblue').fadeOut(4000);
                return false;
            }
        });
        $('#submit').click(function(){
            var emailmsg = $('#email').val();
            if(emailmsg == ""){
                $('#email-error').html('email is also neccessery...! pleae filled it');
                $('#email-error').css('color','red').fadeOut(3000);
                return false;
            }
        });
        $('#submit').click(function(){
            var descrip = $('#text').val();
            if(descrip == ""){
                $('#descrip-error').html('description is must important for us..!');
                $('#descrip-error').css('color','red').fadeOut(3000);
                return false;
            }
        });
    });
</script>
</body>
</html>