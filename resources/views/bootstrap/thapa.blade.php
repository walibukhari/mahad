<!DOCTYPE html>
<html>
<head>
	  <title>Dead Space</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	  <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
</head>
<style>
	*{
		margin: 0;
		padding: 0;
		font-family: 'Josefin Sans', sans-serif;
	}
	.carousel-inner img{
		height: 650px !important;
		width: 100% !important;
	}
	.img-fluid{
		height: 350px !important;
		width: 100% !important;
	}
	.customSetting img{
		border-radius: 5px !important;
	}
	.text-effects:hover{
		transform: rotate(360deg);
	}
	.text-effects{
		text-shadow: 2px 2px 2px blue;
		transition: transform 2s;
		cursor: pointer;
	}
	.buger-contact{
		position: relative;
	}
	.burger_run{
		width: 100px;
		height: 130px;
	}

</style>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Food Resturent Vine</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{route('aboutus-page')}}">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block " src="assets/slicepizza.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block" src="assets/bfries.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block" src="assets/burger.jpg"Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	</header>

	<section>
		<div class="container-fluid">
			<h1 class=" text-effects text-center text-capitalize pt-5">About Us</h1>
			<hr class="w-25 mx-auto pt-5">

			<div class="container my-4 customSetting">
	<div class="row featurette d-flex justify-content-center align-items-center">
          <div class="col-md-7">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5">
            <img class=" img-fluid" data-src="holder.js/350x350/auto" alt="500x500" src="https://source.unsplash.com/350x350?fast,pepsi" data-holder-rendered="true">
          </div>
    </div>
    <div class="row featurette d-flex justify-content-center align-items-center">
          <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5 order-md-1">
            <img class=" img-fluid" data-src="holder.js/350x350/auto" alt="500x500" src="https://source.unsplash.com/350x350?fast,sandwich" data-holder-rendered="true">
          </div>
    </div>
    <div class="row featurette d-flex justify-content-center align-items-center">
          <div class="col-md-7">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5">
            <img class=" img-fluid" data-src="holder.js/350x350/auto" alt="500x500" src="https://source.unsplash.com/350x350?fast,pizza" data-holder-rendered="true">
          </div>
    </div>
</div>
		</div>
	</section>
	<!-- <......Services......> -->
	<section>
		<div class="container-fluid">
			<h1 class=" text-effects text-center text-capitalizet pt-5">Services</h1>
			<hr class="w-25 mx-auto pt-5">
		</div>

        <div class="row ml-3 mb-5">
        	<div class="col-lg-4 col-md-4 col-12">
        		<div class="card" style="width:400px">
				  <img class="card-img-top" src="assets/bm2.jpeg" alt="Card image">
				  <div class="card-body text-center">
				    <h4 class="card-title">Wick John</h4>
				    <p class="card-text">Founder of Shop.</p>
				    <a href="#" class="btn btn-primary"data-toggle="modal" data-target="#myProfile1Modal">See Profile</a>
		         </div>
		         <div class="modal fade" id="myProfile1Modal">
				    <div class="modal-dialog modal-dialog-right">
				      <div class="modal-content">
				      
				        <!-- Modal Header -->
				        <div class="modal-header bg-primary text-white">
				          <h4 class="modal-title">Bio Information</h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        
				        <!-- Modal body -->
				        <div class="modal-body">
				          <form action="/action_page.php">
							  <h2><ins>Name:</ins> <p>Wick John</p><br>
							  	  <ins>Occupation:</ins><p>Owner</p><br>
							  	  <ins>Email:</ins><p>John12@gmail.com</p><br>
							  	  <ins>Address:</ins><p>Street#2 washingtone</p>
							  </h2>
                           </form>
				        </div>
				        
				        <!-- Modal footer -->
				        <div class="modal-footer">
				          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				        </div>
				        
				      </div>
				    </div>
                </div>
                </div>
        	</div>
        	<div class="col-lg-4 col-md-4 col-12">
        		<div class="card" style="width:400px">
				  <img class="card-img-top" src="assets/boss2.jpg" alt="Card image">
				  <div class="card-body text-center">
				    <h4 class="card-title">John Doe</h4>
				    <p class="card-text">Professional Cheif.</p>
				    <a href="#" class="btn btn-success"data-toggle="modal" data-target="#myProfile2Modal">See Profile</a>
		         </div>
		          <div class="modal fade" id="myProfile2Modal">
				    <div class="modal-dialog modal-dialog-right">
				      <div class="modal-content">
				      
				        <!-- Modal Header -->
				        <div class="modal-header bg-success text-white">
				          <h4 class="modal-title">Bio Information</h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        
				        <!-- Modal body -->
				        <div class="modal-body">
				          <form action="/action_page.php">
							  <h2><ins>Name:</ins> <p>John Doe</p><br>
							  	  <ins>Occupation:</ins><p>Cheif</p><br>
							  	  <ins>Email:</ins><p>Doe45@gmail.com</p><br>
							  	  <ins>Address:</ins><p>Street#2 Mexico</p>
							  </h2>
                           </form>
				        </div>
				        
				        <!-- Modal footer -->
				        <div class="modal-footer">
				          <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
				        </div>
				        
				      </div>
				    </div>
                </div>
                </div>
        	</div>
        	<div class="col-lg-4 col-md-4 col-12">
        		<div class="card" style="width:400px">
				  <img class="card-img-top" src="assets/boss3.jpg" alt="Card image">
				  <div class="card-body text-center">
				    <h4 class="card-title">David Alan</h4>
				    <p class="card-text">Professional Cheif.</p>
				    <a href="#" class="btn btn-danger"data-toggle="modal" data-target="#myProfile3Modal">See Profile</a>
		         </div>
		          <div class="modal fade" id="myProfile3Modal">
				    <div class="modal-dialog modal-dialog-right">
				      <div class="modal-content">
				      
				        <!-- Modal Header -->
				        <div class="modal-header bg-danger text-white">
				          <h4 class="modal-title">Bio Information</h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        
				        <!-- Modal body -->
				        <div class="modal-body">
				          <form action="/action_page.php">
							  <h2><ins>Name:</ins> <p>David Alan</p><br>
							  	  <ins>Occupation:</ins><p>Cheif</p><br>
							  	  <ins>Email:</ins><p>Davidalan123@gmail.com</p><br>
							  	  <ins>Address:</ins><p>Street#2 washingtone</p>
							  </h2>
                           </form>
				        </div>
				        
				        <!-- Modal footer -->
				        <div class="modal-footer">
				          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				        </div>
				        
				      </div>
				    </div>
                </div>
                </div>
        	</div>
        </div>
	</section>
<!-- .....contact.... -->
	<section class="bg-primary">
		<article class="py-5 text-center text-light">
			<div>
				<h3 class="display-3">+92 12345678</h3>
				<p>If you want best service then call us now</p>
				<div class="buger-contact">
					<img class="burger_run" src="assets/burgergif.gif" alt="burger-running">
				</div>
				<button class="btn btn-warning">Contact</button>
			</div>
		</article>
	</section>
	<!-- gallery -->

	<section>
		<div class="container-fluid">
			<h1 class=" text-effects text-center text-capitalizet pt-5">Our Food Gallery</h1>
			<hr class="w-25 mx-auto pt-5">
		</div>

		<div class="row mx-5">
			<div class="col-lg-4 col-md-2 col-12 mb-4">
				<img src="assets/opizza.jpg" class="img-fluid">
			</div>
			<div class="col-lg-4 col-md-2 col-12 mb-4">
				<img src="assets/pizza2.jpg" class="img-fluid">
			</div>
			<div class="col-lg-4 col-md-2 col-12 mb-4">
				<img src="assets/ppizza.jpg" class="img-fluid">
			</div>
			<div class="col-lg-4 col-md-2 col-12 mb-4">
				<img src="assets/slice.jpg" class="img-fluid">
			</div>
			<div class="col-lg-4 col-md-2 col-12 mb-4">
				<img src="assets/burger.jpg" class="img-fluid">
			</div>
			<div class="col-lg-4 col-md-2 col-12 mb-4">
				<img src="assets/burger3.jpg" class="img-fluid">
			</div>
		</div>
	</section>
	<!-- .....join now... -->
	<section class="bg-primary">
		<article class="py-5">
			<div class="text-center">
				<h3 class="display-3 text-light">If you want to join</h3>
				<p class="text-light">If you want best service then call us now</p>
				<button class="btn btn-warning" data-toggle="modal" data-target="#myModal">Join Now</button>
			</div>
			<div class="modal fade" id="myModal">
			    <div class="modal-dialog modal-dialog-centered">
			      <div class="modal-content">
			      
			        <!-- Modal Header -->
			        <div class="modal-header">
			          <h4 class="modal-title">Sign Up</h4>
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			        </div>
			        
			        <!-- Modal body -->
			        <div class="modal-body">
			            <form action="#" onsubmit="return validation()">
						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" class="form-control" placeholder="Enter email" id="emailmodel" autocomplete="off">
						  </div>
						  <div id="modelE-error" class="text-danger"></div>
						  <div class="form-group">
						    <label for="pwd">Password:</label>
						    <input type="password" class="form-control" placeholder="Enter password" id="pwdmodel" autocomplete="off">
						  </div>
						  <div id="modalP-error" class="text-danger"></div>
						  <div class="form-group form-check">
						    <label class="form-check-label">
						      <input class="form-check-input" type="checkbox"> Remember me
						    </label>
						  </div>
						  <input type="submit" name="submit" id="submit" value="submit" class="btn btn-primary">
                        </form>
			        </div>
			        
			        <!-- Modal footer -->
			        <div class="modal-footer">
			          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			        </div>
			        
			      </div>
			    </div>
            </div>
		</article>
	</section>

	<!-- .....contact us..... -->

	<section>
		<div class="container-fluid">
			<h1 class=" text-effects text-center text-capitalizet pt-5">Contact</h1>
			<hr class="w-25 mx-auto pt-5">
		</div>

		<div class="w-50 mx-auto mb-5">
			<form action="#" onsubmit="return validation()">
			  <div class="form-group">
			    <label for="name" class="font-weight-bold">Name:</label>
			    <input autocomplete="off" type="text" class="form-control" placeholder="Enter Your Name" id="name">
			    <div id="name-error" class="text-danger"></div>
			  </div>
			  <div class="form-group">
			    <label for="email" class="font-weight-bold">Email address:</label>
			    <input autocomplete="off" type="email" class="form-control" placeholder="Enter email" id="email">
			    <div id="email-error" class="text-danger"></div>
			  </div>
			  <div class="form-group">
			    <label for="number"  class="font-weight-bold">Phone Number:</label>
			    <input type="number" class="form-control" placeholder="Enter Your Number" id="number">
			    <div id="number-error" class="text-danger"></div>
			  </div>
			  <div class="form-group">
			  	<label class="font-weight-bold">Food Description:</label>
			  	<textarea class="form-control"></textarea>
			  </div>
  			    <input type="submit" name="submit" value="submit" autocomplete="off" class="btn btn-danger">  
             </form>
		</div>
	</section>

	<footer>
		<p class="text-center bg-dark text-white">CopyRight FronendDeveloper.com 2020</p>
	</footer>

	<script type="text/javascript">
		var modelemail = document.getElementById('emailmodel').value;
		var modelpassword = document.getElementById('pwdmodel').value;

		function validation()
		{
			   if(modelemail == "")
		    {
		    	document.getElementById('modelE-error').innerHTML="Please filled it...!";
		    	return false;
		    }
		    if(modelpassword == "")
		    {
		    	document.getElementById('modalP-error').innerHTML="Please filled it..!";
		    	return false;
		    }
		}
	</script>

	<script type="text/javascript">
		function validation(){
			var username = document.getElementById('name').value;
			var emailmsg = document.getElementById('email').value;
			var numb = document.getElementById('number').value;

		    if(username == ""){
		    	document.getElementById('name-error').innerHTML="username not empty..!";
		    	return false;
		    }
		    if((username <= 5) || (username >= 12))
		    {
		    	document.getElementById('name-error').innerHTML="length is must between 5 to 12 charactered...!";
		    	return false;
		    }
		    if(emailmsg == ""){
		    	document.getElementById('email-error').innerHTML="please filled it not empty..!";
		    	return false;
		    }
		    if(numb == ""){
		    	document.getElementById('number-error').innerHTML="number is also neccessery..!";
		    	return false;
		    }
		    if((numb <= 5) || (numb >= 12))
		    {
		    	document.getElementById('number-error').innerHTML="digits lenght is must between 5 to 12..!";
		    	return false;
		    }
		}
	</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>