<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    //
    public function front(){

        return view('frontend.index');
    }

    public function second()
    {
        return view('frontend.second');
    }

    public function taj()
    {
    	return view('frontend.taj');
    }

    public function bootstrap()
    {
        return view('bootstrap.index');
    }

    public function beard()
    {
        return view('frontend.beard');
    }

    public function last()
    {
        return view('frontend.last');
    }

    public function isi()
    {
        return view('frontend.isi');
    }

    public function barber()
    {
        return view('frontend.barber');
    }

    public  function website()
    {
        return view('bootstrap.boostrap');
    }
    public  function hello()
    {
        return view('bootstrap.tutorial');
    }

    public function about()
    {
        return view('bootstrap.about');
    }

    public function contact()
    {
        return view('bootstrap.contact');
    }

    public function thapa()
    {
        return view('bootstrap.thapa');
    }
    public function jquery()
    {
        return view('frontend.jquery');
    }
    public function aboutus(){
        return view('bootstrap.about2');
    }
    public function java()
    {
        return view('bootstrap.java');
    }
    public function finalpage()
    {
        return view('bootstrap.final');
    }
    public function finallypage()
    {
        return view('bootstrap.finally');
    }
    public function finalepage()
    {
        return view('bootstrap.finallast');
    }

}
