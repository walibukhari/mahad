<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/create-page','FrontEndController@front')->name('create-page');

Route::get('/second-page','FrontEndController@second')->name('second-page');
Route::get('/taj-page','FrontEndController@taj')->name('taj-page');
Route::get('/trap-page','FrontEndController@bootstrap')->name('trap-page');
Route::get('/men-page','FrontEndController@beard')->name('men-page');
Route::get('/last-page','FrontEndController@last')->name('last-page');
Route::get('/isi-page','FrontEndController@isi')->name('isi-page');
Route::get('/barber-page','FrontEndController@barber')->name('barber-page');
Route::get('/bootstrap-page','FrontEndController@website')->name('bootstrap-page');
Route::get('/hello-page','FrontEndController@hello')->name('hello-page');
Route::get('/about-page','FrontEndController@about')->name('about-page');
Route::get('/contact-page','FrontEndController@contact')->name('contact-page');
Route::get('/thapa-page','FrontEndController@thapa')->name('thapa.page');
// .......jquery.......
Route::get('jquery-page','FrontEndController@jquery')->name('jquery-page');
Route::get('aboutus-page','FrontEndController@aboutus')->name('aboutus-page');
Route::get('java-page','FrontEndController@java')->name('java-page');
Route::get('finally-page','FrontEndController@finallypage')->name('finally-page');

// .....demo websites.....
Route::get('demo-website','FrontEndController@finalpage')->name('demo-website');
Route::get('custom-website','FrontEndController@finalepage')->name('custome-website');
